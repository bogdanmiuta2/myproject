package org.fasttrackit;

public class Main {
    public static void main(String[] args) {

        System.out.println("Swag Labs Final Project");
        System.out.println("1. User can login with valid credentials");
        LoginPage page = new LoginPage();
        page.openHomePage();
        LoginPage login = new LoginPage();
        login.typeInUsername("standard_user");
        login.typeInPassword("secret_sauce");
        login.clickOnTheLoginButton();
    }
}
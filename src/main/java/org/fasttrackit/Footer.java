package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Footer {
    private static final String TwitterURL = "https://twitter.com/saucelabs";
    private static final String FacebookURL = "https://www.facebook.com/saucelabs";
    private static final String LinkedinURL = "https://www.linkedin.com/company/sauce-labs/";
    public final SelenideElement twitter = $(".social_twitter");
    public final SelenideElement facebook = $(".social_facebook");
    public final SelenideElement linkedin = $(".social_linkedin");
    public final SelenideElement footertext = $(".footer_copy");

    public void openTwitterPageSauceLabs() {
        System.out.println("Twitter page of Sauce Labs is open");
        twitter.click();
        open(TwitterURL);
    }
    public boolean isOpeningTwitterPage() {
        return twitter.isDisplayed();
    }
    public boolean isOpeningFacebookPage() {
        return facebook.isDisplayed();
    }
    public boolean isOpeningLinkedinPage() {
        return linkedin.isDisplayed();
    }

    public void openFacebookPageSauceLabs() {
        System.out.println("Facebook page of Sauce Labs is open");
        facebook.click();
        open(FacebookURL);
    }

    public void openLinkedinPageSauceLabs() {
        System.out.println("Linkedin page of Sauce Labs is open");
        linkedin.click();
        open(LinkedinURL);
    }
    public String getSeeTheFooterText() {
        return footertext.text();
    }

}

package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.Objects;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SecondaryHeader {
    private final SelenideElement pageTitle = $(".title");
    private final ElementsCollection cards = $$(".inventory_list");
    private final SelenideElement sortButton = $(".product_sort_container");
    private final SelenideElement sortAZ = $("option[value=az]");
    private final SelenideElement sortZA = $("option[value=za]");
    ;
    private final SelenideElement sortLoHi = $("option[value=lohi]");
    ;
    private final SelenideElement sortHiLo = $("option[value=hilo]");

    public String getPageTitle() {
        return pageTitle.text();
    }

    public void clickOnTheSortButton() {
        System.out.println("Sort-Button activated");
        this.sortButton.click();
    }

    public void clickOnTheAZSortButton() {
        System.out.println("Click on the Sort AZ");
        this.sortAZ.click();
    }

    public void clickOnTheZASortButton() {
        System.out.println("Click on the Sort ZA");
        this.sortZA.click();
    }

    public void clickOnTheSortByPriceLoHi() {
        System.out.println("Click on the Sort by price LoHi");
        this.sortLoHi.click();
    }

    public void clickOnTheSortByPriceHiLo() {
        System.out.println("Click on the Sort by price HiLo");
        this.sortHiLo.click();
    }

    public Product getFirstProductInList() {
        SelenideElement first = cards.first();
        return new Product(first);
    }

    public Product getLastProductInList() {
        SelenideElement last = cards.last();
        return new Product(last);
    }
    public Product getProductById(String productId) {
        for (SelenideElement product : cards) {
            if (Objects.requireNonNull(product.$(".inventory_item_label").getAttribute("id=")).endsWith("item_" + productId + "_title_link")) {
                return new Product(product);
            }
        }
        return null;
    }
}

package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.sql.Array;
import java.util.ArrayList;
import java.util.EnumSet;

import static com.codeborne.selenide.Selenide.*;

public class CheckoutPage extends CartPage {
    private final SelenideElement infoUser = $(".checkout_info");
    private final SelenideElement firstName = $(".form_group #first-name");
    private final SelenideElement lastName = $(".form_group #last-name");
    private final SelenideElement postalCode = $(".form_group #postal-code");
    private final SelenideElement errorMessage = $(".error-message-container error");
    private final SelenideElement cancelButton = $(".cart_cancel_link");
    private final SelenideElement continueButton = $(".cart_button");
    private final SelenideElement checkoutOverview = $(".checkout_summary_container");
    private final SelenideElement titleOptionsName = $(".summary_info_label");
    private final SelenideElement valueOptionsName = $(".summary_value_label");
    private final SelenideElement subTotal = $(".summary_subtotal_label");
    private final SelenideElement theTax = $(".summary_tax_label");

    public boolean isFormForCompletingInformations() {
        return infoUser.isDisplayed();
    }

    public String getFirstName() {
        infoUser.click();
        firstName.val("Bogdan");
        return firstName.val();
    }

    public String getLastName() {
        infoUser.click();
        lastName.val("Miuta");
        return lastName.val();
    }

    public String getPostalCode() {
        infoUser.click();
        postalCode.val("000000");
        return postalCode.val();
    }

    public boolean isErrorMessageVisible() {
        return !errorMessage.isDisplayed();
    }

    public void clickOnTheCancel() {
        System.out.println("Back to Cart Page");
        cancelButton.click();
    }

    public void clickOnTheContinue() {
        System.out.println("Continue Step Two");
        continueButton.click();
    }

    public boolean isOptionsNameVisible() {
        return titleOptionsName.isDisplayed();
    }

    public boolean isValuesNameVisible() {
        return valueOptionsName.isDisplayed();
    }


    public String getTitleNameOptions() {
        titleOptionsName.val("Payment Information");
        titleOptionsName.val("Shipping Information");
        titleOptionsName.val("Price Total");
        titleOptionsName.val("Total: $");
        return titleOptionsName.val();
    }

    public String getValueNameOptions() {
        valueOptionsName.setValue("SauceCard #31337");
        valueOptionsName.setValue("Free Pony Express Delivery!");
        return valueOptionsName.val();
    }

    @Override
    public String toString() {
        return titleOptionsName + "Title" + valueOptionsName + "Value";
    }

    public boolean isCheckoutStepTwoPageVisible() {
        return checkoutOverview.isDisplayed();
    }

    public String getSubTotal() {
        return this.subTotal.text();
    }

    public String getTheTax() {
        return this.theTax.text();
    }
}

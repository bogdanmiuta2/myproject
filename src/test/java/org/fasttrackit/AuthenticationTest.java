package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import jdk.jfr.Description;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
@Test
public class AuthenticationTest {
    LoginPage login = new LoginPage();
    LoginPage page = new LoginPage();
    BurgerMenu burger = new BurgerMenu();
    InvalidAccount invalid = new InvalidAccount("locked_out_user", "secret_sauce", "Epic sadface: Sorry, this user has been locked out.");

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Selenide.refresh();
        Selenide.clearBrowserCookies();

    }

    @Description("User can login with valid credentials")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Bogdan Miuta")
    @Lead("Bogdan")
    @Link(name = "Swag Labs", url = "https://www.saucedemo.com/")
    @Issue("DMS-001")
    @TmsLink("https://www.saucedemo.com//issues/DMS-001")
    @Story("Login with valid Credentials")
    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    public void valid_user_can_login_with_valid_credentials(Account account) {
        login.typeInUsername(account.getUser());
        login.typeInPassword(account.getPassword());
        login.clickOnTheLoginButton();
        burger.clickToOpenMenu();
        burger.clickToLogout();
    }

    @Test(dataProvider = "invalidCredentials", dataProviderClass = AuthenticationDataProvider.class)
    public void non_valid_logins_with_invalid_credentials(Account account) {
        login.typeInUsername(account.getUser());
        login.typeInPassword(account.getPassword());
        login.clickOnTheLoginButton();
        boolean errorMessageVisible = !login.isErrorMessageVisible();
        String errorMsg = invalid.getErrorMsg();
        assertTrue(errorMessageVisible, "Error message is shown when invalid credentials used for login");
        assertEquals(errorMsg, invalid.getErrorMsg());
    }
}

package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ErrorUserTest {
    LoginPage page = new LoginPage();
    SecondaryHeader header2;
    PrimaryHeader header;
    CartPage cart;
    Product product;
    BurgerMenu burger;
    CheckoutPage checkout;


    @BeforeTest
    public void setup() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();

        header = new PrimaryHeader();
        header2= new SecondaryHeader();
        cart = new CartPage();
        product = new Product("4");
        burger = new BurgerMenu();
        checkout = new CheckoutPage();
    }
    @AfterMethod
    public void cleanup() {
        burger.clickToOpenMenu();
        burger.clickToReset();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
    }
    @Test
    public void error_user_can_login_with_valid_credentials() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
    }

    @Test
    public void error_user_can_navigate_to_cart_page() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        assertEquals(header2.getPageTitle(), "Your Cart", "Expected to be on the Cart page");
    }

    @Test
    public void error_user_can_navigate_to_homepage_from_cart_page() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        cart.clickToBackHPFromCP();
        assertEquals(header2.getPageTitle(), "Products", "Expected to be on Products page.");
    }

    @Test
    public void error_user_can_add_product_to_cart_from_product_page() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        product.clickOnAddToCart();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test
    public void error_user_do_not_can_add_product_to_cart_from_product_page() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        product.clickOnAddToCart();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue,"1", "After adding one product to cart, badge shows nothing");
    }

    @Test
    public void error_user_do_not_can_remove_product_from_product_page() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        product.clickOnAddToCart();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        product.clickOnRemoveButton();
        assertEquals(badgeValue,"1","After adding one product to cart, badge shows 1, but can't remove");
    }
    @Test
    public void error_user_can_completing_info_on_checkout_page() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        cart.clickToCheckout();
        assertTrue(checkout.isFormForCompletingInformations());
        String formFirstName = checkout.getFirstName();
        String formLastName = checkout.getLastName();
        String formPostalCode = checkout.getPostalCode();

        assertEquals(formFirstName, "Bogdan" ,"This field should be fill with your first name");
        assertEquals(formLastName, "", "This field should be fill with your last name");
        assertEquals(formPostalCode, "000000", "This field should be fill with postal code");
    }
    @Test
    @Ignore
    public void error_user_can_see_info_and_value_labels_from_checkout_overview_page() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        cart.clickToCheckout();
        assertTrue(checkout.isFormForCompletingInformations());
        String firstName = checkout.getFirstName();
        String lastName = checkout.getLastName();
        String postalCode = checkout.getPostalCode();
        checkout.clickOnTheContinue();
        assertTrue(checkout.isOptionsNameVisible());
        String titleoption = checkout.getTitleNameOptions();
        assertTrue(checkout.isValuesNameVisible());
        String valueoption = checkout.getValueNameOptions();
        assertTrue(checkout.isCheckoutStepTwoPageVisible());
        String labelSubTotal = checkout.getSubTotal();
        String labelTax = checkout.getTheTax();

        assertEquals(firstName,"Bogdan");
        assertEquals(lastName,"");
        assertEquals(postalCode,"000000");

        assertEquals(titleoption,"Payment Information");
        assertEquals(titleoption,"Shipping Information");
        assertEquals(titleoption,"Price Total");
        assertEquals(titleoption,"Total: $");

        assertEquals(valueoption,"SauceCard #31337");
        assertEquals(valueoption,"Free Pony Express Delivery!");

        assertEquals(labelSubTotal,"Item total: $");
        assertEquals(labelTax,"Tax: $");
    }
    @Test
    public void error_user_can_see_error_message_from_checkout_page_info() {
        page.openHomePage();
        page.typeInUsername("error_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        cart.clickToCheckout();
        checkout.clickOnTheContinue();
        boolean errorMessageVisible = checkout.isErrorMessageVisible();
        assertTrue(errorMessageVisible,"Error message is shown when is not completed the form information");
    }



}

package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import java.net.URL;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class BurgerMenu {

    public static final String URL = "https://saucelabs.com/";
    private final SelenideElement openMenu = $(".bm-burger-button");
    private final SelenideElement openAllItems = $(".bm-item-list #inventory_sidebar_link");
    private final SelenideElement openAbout = $(".bm-item-list #about_sidebar_link");
    private final SelenideElement logoutAccount = $(".bm-item-list #logout_sidebar_link");
    private final SelenideElement resetAppState = $(".bm-item-list #reset_sidebar_link");
    private final SelenideElement closeMenu = $(".bm-cross-button");

    public void clickToOpenMenu() {
        openMenu.click();
        System.out.println("Menu open");
    }

    public void clickToAllItems() {
        openAllItems.click();
    }

    public void clickToAbout() {
        openAbout.click();
        System.out.println("Main site is opening...");
        open(URL);
    }

    public void clickToLogout() {
        logoutAccount.click();
        System.out.println("Logout");
    }

    public void clickToReset() {
        resetAppState.click();
        System.out.println("Reset App State");
    }

    public void clickToCloseMenu() {
        closeMenu.click();
        System.out.println("Menu closed!");
    }
}

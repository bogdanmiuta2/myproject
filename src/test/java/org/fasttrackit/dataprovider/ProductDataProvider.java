package org.fasttrackit.dataprovider;

import org.fasttrackit.ProductData;
import org.testng.annotations.DataProvider;

public class ProductDataProvider {
    @DataProvider(name = "validCredentials")
    public Object[][] getCredentials() {
        ProductData agc = new ProductData("4", "Sauce Labs Backpack","$29.99");
        ProductData ich = new ProductData("1","Sauce Labs Bolt T-Shirt","$15.99");

        return new Object[][]{
                {"standard_user","secret_sauce"},
                {"error_user","secret_sauce"},
                {"problem_user","secret_sauce"},
        };
    }
}

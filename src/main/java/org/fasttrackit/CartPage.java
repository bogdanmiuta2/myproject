package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement emptyCartPageElement = $(".cart_list");
    private final SelenideElement cartPageElement = $(".cart_item");
    private final SelenideElement backButtonToHomePageFromCartPage = $(".back");
    private final SelenideElement checkoutButton = $(".checkout_button");

    public CartPage() {

    }

    public String getEmptyCartPageText() {
        return this.emptyCartPageElement.text();
    }

    public boolean isEmptyCartMessageDisplayed() {
        return emptyCartPageElement.isDisplayed();
    }

    public String getCartPageText() {
        return this.cartPageElement.text();
    }
    public void clickToBackHPFromCP() {
        backButtonToHomePageFromCartPage.click();
        System.out.println("Should be in Home Page");
    }
    public void clickToCheckout() {
        checkoutButton.click();
        System.out.println("Enjoy Checkout!");
    }
}

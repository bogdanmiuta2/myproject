package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage {
    public static final String URL = "https://www.saucedemo.com/";
    public static final String backPCURL = "https://www.saucedemo.com/inventory.html";
    private final SelenideElement userName = $(".login-box #user-name");
    private final SelenideElement password = $(".login-box #password");
    private final SelenideElement errorMessage = $(".error-message-container error");
    private final SelenideElement loginButton = $(".submit-button");

    @Step("Open HomePage ")
    public void openHomePage() {
        System.out.println("Opening : " + URL);
        open(URL);
    }

    public void reloadProductCardsPage() {
        System.out.println("Reload Product Page");
        open(backPCURL);
    }

    @Step("Type in username")
    public void typeInUsername(String user) {
        System.out.println("Click on the Username field");
        userName.click();
        System.out.println("Type in " + user);
        userName.type(user);

    }

    @Step("Type in password")
    public void typeInPassword(String pass) {
        System.out.println("Click on the Password field.");
        password.click();
        System.out.println("Type in" + pass);
        password.type(pass);
    }

    public boolean isErrorMessageVisible() {
        return errorMessage.isDisplayed();
    }


    @Step("Click on the Login button.")
    public void clickOnTheLoginButton() {
        System.out.println("Click On The Login button.");
        loginButton.click();
    }
}

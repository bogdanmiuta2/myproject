package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class FooterTest {

    LoginPage page = new LoginPage();
    Footer footer = new Footer();

    @BeforeClass
    public void setup() {
        page.openHomePage();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
    }

    @AfterMethod
    public void cleanup() {
        Selenide.refresh();
        Selenide.clearBrowserCookies();
    }

    @Test
    public void users_can_navigate_to_twitter_page() {
        page.openHomePage();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        footer.openTwitterPageSauceLabs();
        page.reloadProductCardsPage();

        assertTrue(footer.isOpeningTwitterPage(),"Opening Twitter Page");
    }
    @Test
    public void users_can_navigate_to_facebook_page() {
        footer.openFacebookPageSauceLabs();
        page.reloadProductCardsPage();

        assertTrue(footer.isOpeningFacebookPage(),"Opening Facebook Page");
    }
    @Test
    public void users_can_navigate_to_linkedin_page() {
        page.openHomePage();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        footer.openLinkedinPageSauceLabs();
        page.reloadProductCardsPage();

        assertTrue(footer.isOpeningLinkedinPage(),"Opening Linkedin Page");
    }
    @Test
    public void users_can_reading_footer_text() {
        String footertext = footer.getSeeTheFooterText();

        assertEquals(footertext, "© 2023 Sauce Labs. All Rights Reserved. Terms of Service | Privacy Policy");
    }
}

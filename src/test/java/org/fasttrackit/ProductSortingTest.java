package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ProductSortingTest {
    LoginPage page = new LoginPage();
    SecondaryHeader header2 = new SecondaryHeader();

    @BeforeClass
    public void setup() {
        page.openHomePage();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
    }

    @AfterMethod
    public void cleanup() {
        Selenide.refresh();
        Selenide.clearBrowserCookies();

    }

    @Test
    public void when_sorting_products_a_to_z_products_are_sorted_alphabetically_ASC() {
        Product firstProductBeforeSort = header2.getFirstProductInList();
        Product lastProductBeforeSort = header2.getLastProductInList();
        header2.clickOnTheSortButton();
        header2.clickOnTheAZSortButton();
        Product firstProductAfterSort = header2.getFirstProductInList();
        Product lastProductAfterSort = header2.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
    }


    @Test
    public void when_sorting_products_z_to_a_products_are_sorted_alphabetically_DESC() {
        header2.clickOnTheZASortButton();
        Product firstProductBeforeSort = header2.getFirstProductInList();
        Product lastProductBeforeSort = header2.getLastProductInList();
        header2.clickOnTheSortButton();
        header2.clickOnTheZASortButton();
        Product firstProductAfterSort = header2.getFirstProductInList();
        Product lastProductAfterSort = header2.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
    }

    @Test
    public void when_sorting_products_by_price_low_to_high_products_are_sorted_by_price_ASC() {
        header2.clickOnTheSortButton();
        header2.clickOnTheSortByPriceLoHi();
        Product firstProductBeforeSort = header2.getFirstProductInList();
        Product lastProductBeforeSort = header2.getLastProductInList();
        header2.clickOnTheSortButton();
        header2.clickOnTheSortByPriceLoHi();
        Product firstProductAfterSort = header2.getFirstProductInList();
        Product lastProductAfterSort = header2.getLastProductInList();


        assertEquals(firstProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        page.openHomePage();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
    }
     @Test
    public void when_sorting_products_by_price_high_to_low_products_are_sorted_by_price_DESC() {
        header2.clickOnTheSortButton();
        header2.clickOnTheSortByPriceHiLo();
        Product firstProductBeforeSort = header2.getFirstProductInList();
        Product lastProductBeforeSort = header2.getLastProductInList();
        header2.clickOnTheSortButton();
        header2.clickOnTheSortByPriceHiLo();
        Product lastProductAfterSort = header2.getLastProductInList();
        Product firstProductAfterSort = header2.getFirstProductInList();


        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
         page.openHomePage();
         page.typeInUsername("standard_user");
         page.typeInPassword("secret_sauce");
         page.clickOnTheLoginButton();
    }
}

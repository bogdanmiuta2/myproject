package org.fasttrackit.dataprovider;

import org.fasttrackit.Account;
import org.fasttrackit.InvalidAccount;
import org.testng.annotations.DataProvider;

public class AuthenticationDataProvider {


    @DataProvider(name = "validCredentials")
    public Object[][] getCredentials() {
        Account standard = new Account("standard_user", "secret_sauce");
        Account error = new Account("error_user", "secret_sauce");
        Account problem = new Account("problem_user", "secret_sauce");

        return new Object[][]{
                {standard},
                {error},
                {problem},
        };

    }

    @DataProvider(name = "invalidCredentials")
    public Object[][] invalidCredentials() {
        InvalidAccount locked = new InvalidAccount("locked_out_user", "secret_sauce", "Epic sadface: Sorry, this user has been locked out.");
        InvalidAccount unknownUser = new InvalidAccount("unknown", "secret_sauce", "Epic sadface: Username and password do not match any user in this service");
        InvalidAccount wrongPass = new InvalidAccount("standard_user", "wrongpassword", "Epic sadface: Username and password do not match any user in this service");
        InvalidAccount noPassword = new InvalidAccount("standard_user", "", "Epic sadface: Password is required");
        InvalidAccount noUsername = new InvalidAccount("", "secret_sauce", "Epic sadface: Username is required");
        return new Object[][]{
                {locked},
                {unknownUser},
                {wrongPass},
                {noPassword},
                {noUsername}
        };
    }
}


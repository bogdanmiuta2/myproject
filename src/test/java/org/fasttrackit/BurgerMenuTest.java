package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BurgerMenuTest extends BurgerMenu {
    LoginPage page = new LoginPage();
    BurgerMenu burger = new BurgerMenu();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        burger.clickToOpenMenu();
        burger.clickToReset();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
    }

    @Test
    public void user_can_click_to_all_items() {
        page.openHomePage();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        burger.clickToOpenMenu();
        burger.clickToAllItems();
        burger.clickToCloseMenu();
        page.reloadProductCardsPage();
    }

    @Test
    public void user_can_click_to_about_button() {
        page.openHomePage();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        burger.clickToOpenMenu();
        burger.clickToAbout();
        page.reloadProductCardsPage();

    }

    @Test
    public void user_can_logout_from_burger_menu() {
        burger.clickToOpenMenu();
        burger.clickToLogout();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
    }

    @Test
    public void user_can_reset_app_state_from_burger_menu() {
        page.openHomePage();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        burger.clickToOpenMenu();
        burger.clickToReset();
        burger.clickToCloseMenu();
    }

    @Test
    public void all_users_can_do_in_burger_menu() {
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        burger.clickToOpenMenu();
        burger.clickToAllItems();
        page.reloadProductCardsPage();
        burger.clickToOpenMenu();
        burger.clickToAbout();
        page.reloadProductCardsPage();
        burger.clickToOpenMenu();
        burger.clickToLogout();
        page.typeInUsername("standard_user");
        page.typeInPassword("secret_sauce");
        page.clickOnTheLoginButton();
        burger.clickToOpenMenu();
        burger.clickToReset();
        burger.clickToCloseMenu();
    }

}
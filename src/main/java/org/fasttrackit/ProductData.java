package org.fasttrackit;

public class ProductData {
    private final String productId;
    private final String name;
    private final String price;

    public ProductData(String productId, String name, String price) {
        this.productId = productId;
        this.name = name;
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }
    @Override
    public String toString() {
        return this.name;
    }
}

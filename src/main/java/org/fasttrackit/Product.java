package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement card;
    private final SelenideElement addToCartButton;
    private final SelenideElement removeButton;
    private final String title;
    private final String price;

    public Product(SelenideElement card) {
        this.card = card;
        this.addToCartButton = card.$(".pricebar .btn_inventory");
        this.removeButton = card.$(".pricebar .btn_inventory");
        this.title = this.card.$(".inventory_item_name").text();
        this.price = this.card.$(".inventory_item_price").text();

    }

    public Product(String productId) {
        String productIdSelector = String.format("[id='item_%s_title_link']", productId);
        SelenideElement inventory_item = $(productIdSelector);
        this.card = inventory_item.parent().parent();
        this.addToCartButton = card.$(".pricebar .btn_inventory");
        this.removeButton =card.$(".pricebar .btn_inventory");
        this.title = inventory_item.text();
        this.price = this.card.$(".inventory_item_price").text();
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public void clickOnAddToCart() {
        System.out.println("Click on Add to cart");
        this.addToCartButton.click();
    }
    public void clickOnRemoveButton() {
        System.out.println("Click on the remove button");
        this.removeButton.click();
    }
}

